import en from "../../assets/locale/en.json"; //default

const STORE_KEY = "app.locale";

export const defaultLocale = 'en';
export const locales = ['en','fr'];
export const defaultMessages : Record<string,string> = en;


export const getLocale = () : string => {return (localStorage.getItem(STORE_KEY) || navigator.language).substring(0,2).toLowerCase();};

export const setLocale = async (lang : string) => localStorage.setItem(STORE_KEY,lang);

export const removeLocale = async () => localStorage.removeItem(STORE_KEY);

export const getMessages = async (lang : string) : Promise<Record<string,string>> => {
  if (lang.toLowerCase() === defaultLocale) {
    return defaultMessages;
  } else {
    const response = await fetch('locale/'+ lang + '.json');
    if (response.ok) {
      return response.json();
    } else {
      console.error(`Could not get messages for lang=${lang}`);
      return defaultMessages; 
    }
  }
}