import React,{useState,useEffect} from 'react';
import { dfx_base_backend } from '../../../declarations/dfx_base_backend';
import {whoami,isAutenticated,login,logout} from '../services/auth'
import {FormattedMessage,useIntl} from "react-intl";

import '../../assets/main.css'

const Dfinity = () => {
  const intl = useIntl();
  const [greeting,setGreeting] = useState('');
  const [name,setName] = useState('');
  const [principal,setPrincipal] = useState('');
  const [authenticated,setAuthenticated] = useState(false);

  useEffect(() => {
    whoami().then((response)=>setPrincipal(response));
    isAutenticated().then((response)=>setAuthenticated(response));
  },[principal,authenticated]);
  
  const handelSubmit = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    setGreeting(await dfx_base_backend.greet(name));
  }

  const handelLogin = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    if (authenticated) {
      logout().then(()=>{
        alert("Logout finished !");
        setPrincipal('');
    });
    } else {
      login(()=>{
        alert("Login finished !");
        setPrincipal('');
      });
    }
  }
  
  return (
    <main>
      <img src="logo2.svg" alt="DFINITY logo" />
      <form action="#">
        <label htmlFor="name"><FormattedMessage defaultMessage='Enter your name: ' id='key.dfinity.label.name'/></label>
        <input id="name" alt="Name" type="text" value={name} onChange={(e)=>{setName(e.target.value)}}/>
        <button type="submit" onClick={(e)=>handelSubmit(e)}><FormattedMessage defaultMessage='Click Me!' id='key.dfinity.btn.submit'/></button>
      </form>
      <section id="greeting">{greeting}</section>
      <section id="greeting">
        <span><FormattedMessage defaultMessage='Who am I ?' id='key.dfinity.whoami.msg'/> </span>
        <span>{principal}</span>
      </section>
      <section id="greeting">
        <span><FormattedMessage defaultMessage='Is authenticated ? {r}' values={{r:authenticated.toString()}} id='key.dfinity.authenticated.state'/></span>
        <div><button type="button" onClick={(e)=>handelLogin(e)}>{authenticated === true ? intl.formatMessage({ id: 'key.dfinity.logout',defaultMessage: 'Logout'}) : intl.formatMessage({ id: 'key.dfinity.login',defaultMessage: 'Login'})}</button></div>
      </section>
    </main>
  );
}

export default Dfinity;